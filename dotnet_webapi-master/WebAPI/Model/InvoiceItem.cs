﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebAPI.FirstExercice.Model
{
    public class InvoiceItem
    {
        [Key]
        public int InvoiceItemId { get; set; }
        public int InvoiceId { get; set; }
        public string Code { get; set; }
        public decimal Price { get; set; }
        public decimal Desc { get; set; }
        public int Quantity { get; set; }
        [ForeignKey("InvoiceId")]
        public virtual Invoice Invoice { get; set; }
        public int idIva {get;set;}
                [ForeignKey("idIva")]
        public virtual IVA IVA { get; set; }
    }
}
