using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace WebAPI.FirstExercice.Model
{
    public class IVA
       {
        [Key]
        public int idIva { get; set; }
        public string descricao { get; set; }
        public double taxa { get; set; }
        public DateTime dataInicio { get; set; }
        public DateTime dataFim { get; set; }

    }
}
