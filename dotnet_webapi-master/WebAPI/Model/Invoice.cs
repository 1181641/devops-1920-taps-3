﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace WebAPI.FirstExercice.Model
{
    public class Invoice
    {

        public Invoice(){
            this.Id = 0;
            this.Date = DateTime.UtcNow;
            this.CustomerId = 0;
        }

        [Key]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string InvoiceCode { get; set; }
        public int CustomerId { get; set; }

        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }
        public virtual List<InvoiceItem> Items { get; set; }

        public Boolean validarSemLinhas(){
            return (Items.Count == 0);
        }

        public Boolean validarValoresNegativos(){
            if (Items.Count > 0){
            foreach (InvoiceItem item in Items){
                    if (item.Price < 0){
                        return true;
                    }
                }
            }
            return false;
        }

        public Boolean validarTotalFatura(decimal total){
            return (total == Items.Sum(x => x.Quantity * x.Price));
        }
    }
}
