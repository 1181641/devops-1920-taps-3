﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebAPI.FirstExercice.Model
{
    public class Customer
    {
        [Key]
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Contact { get; set; }
        public virtual List<Invoice> Invoices { get; set; }
    }
}
