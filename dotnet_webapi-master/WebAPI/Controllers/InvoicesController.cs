﻿using WebAPI.FirstExercice.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.FirstExercice.Model;
using System;
using Microsoft.EntityFrameworkCore;

namespace WebAPI.FirstExercice.Controllers
{
    [Produces("application/json")]
    [Route("v1/[controller]")]
    [ApiController]
    public class InvoicesController : ControllerBase
    {

        private readonly DbApiContext _context;
        public InvoicesController(DbApiContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var response = _context.Invoices.Select(
                   (i => new
                   {
                       i.InvoiceCode,
                       Date = i.Date.ToString("dd/MM/yyyy"),
                       CustomerName = i.Customer.FirstName + i.Customer.LastName,
                       CustomerContact = i.Customer.Contact,
                       TotalItens = i.Items.Sum(x => x.Quantity),
                       TotalValue = i.Items.Sum(x => x.Quantity * x.Price)
                   }));

            return Ok(response);
        }

        [HttpPost]
        public async Task<ActionResult<Invoice>> Add(Invoice fatura)
        {
            if (fatura == null)
            {
                return BadRequest();
            }
            try
            {

                if (fatura.InvoiceCode != ""){
                    fatura.Id = await _context.Invoices.MaxAsync(x => x.Id) + 1;
                }else {
                    return BadRequest();
                }
                
                if (fatura.CustomerId != 0){
                    fatura.Customer = _context.Customers.Single(x => (x.CustomerId == fatura.CustomerId));
                }else{
                    return BadRequest();
                }

                if(fatura.Items.Count !=0) {
                    int idItem = await _context.InvoiceItems.MaxAsync(x => x.InvoiceItemId) + 1;
                    foreach (InvoiceItem item in fatura.Items){
                        idItem++;
                        item.InvoiceId = fatura.Id;
                        item.InvoiceItemId = idItem;
                        if (item.idIva != 0){
                            item.IVA = _context.Iva.Single(x => (x.idIva == item.idIva));
                        }else{
                            return BadRequest();
                        }
                    }
                }else{
                    return BadRequest();
                }

                await _context.Invoices.AddAsync(fatura);
                await _context.SaveChangesAsync();

                var response = new
                {
                    newFatura = "CENAS"//fatura.InvoiceCode, fatura.Customer, fatura.Items
                };
                return Ok(response);
            }
            catch(Exception)
            {
                return BadRequest();
            }
        }

    }
}
