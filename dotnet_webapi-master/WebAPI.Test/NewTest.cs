using Xunit;

namespace WebAPI.Test
{
    public class NewTest
    {
        private readonly FirstExercice.Model.Invoice _FirstExercice;

        public NewTest(){
            _FirstExercice = new FirstExercice.Model.Invoice();
        }

        [Fact]
        public void FirstTest(){

            var result = _FirstExercice.validarSemLinhas();
            Assert.False(result, "Invoice sem linhas!");

            result = _FirstExercice.validarValoresNegativos();
            Assert.False(result, "Invoice com valores negativos!");

            result = _FirstExercice.validarTotalFatura(10);
            Assert.True(result, "Invoice com totais diferentes!");
        }
    }

}